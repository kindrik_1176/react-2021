



import React, { useState } from 'react';
import uuid from 'uuid/dist/v1';

const Songlist = () => {
    const [songs, setSongs] = useState([
        {title:'Long Live The Chief', id: 1},
        {title:'Red Right Hand', id: 2},
        {title:'Ding Ding Doll', id: 3}
    ]);
    const addSong = () => {
        setSongs([...songs, {title:'New Song', id: uuid()}])
    }
    return ( 
        <div className='song-list'>
            <ul>
                {songs.map(song => {
                    return(
                        <li key={song.id}>{song.title}</li>
                    )
                })}
            </ul>
            <button onClick={addSong}>Add a song</button>
        </div>
     );
}
 
export default Songlist;