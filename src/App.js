

import React from 'react';
import Booklist from './components/Booklist';
import Navbar from './components/Navbar';
import Songlist from './components/Songlist';
import ThemeToggle from './components/ThemeToggle';
import AuthContextProvider from './contexts/AuthContext';
import ThemeContextProvider from './contexts/ThemeContext';

function App() {
  return (
    <div className="App">
      {/* <ThemeContextProvider>
        <AuthContextProvider>
          <Navbar />
          <Booklist />
          <ThemeToggle />
        </AuthContextProvider>
      </ThemeContextProvider> */}

      <Songlist />


    </div>
  );
}

export default App;
